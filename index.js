/*
	1. Create a function that checks whether a number is odd or even.
*/

	function getOddEven(){

		let number = prompt("Hi! Please enter a number: ");
		
		if(number % 2 == 0) {
		    console.log(number + " is an even number.");
		}

		else if(number % 2 == 1) {
		    console.log(number + " is an odd number.");
		}

		else
			alert("Please refresh page and input a number.");
		
	};

	getOddEven();


/*
	2. Create a function that displays a user's top 3 favorite movie characters. Use a prompt to gather user input.
*/

	function enterMovChar(){
		alert("Please enter the names of your top 3 favorite movie characters.");
	};

		enterMovChar();


	let printCharacters = function enterFaveChar(){
		let character1 = prompt("Enter your first favorite movie character's name:"); 
		let character2 = prompt("Enter your second favorite movie character's name:"); 
		let character3 = prompt("Enter your third favorite movie character's name:");

		console.log("Your favorite movie characters are:")
		console.log('1. ' + character1); 
		console.log('2. ' + character2); 
		console.log('3. ' + character3); 
	};

	printCharacters();


/*
	3. Write a function that computes a user's yearly income.
*/

	function printYearlyIncome(){

		let monthlyIncome = prompt("Input your monthly income. ");
		console.log('Your yearly income is ' + monthlyIncome * 12 + '.');
		
	};

	printYearlyIncome();